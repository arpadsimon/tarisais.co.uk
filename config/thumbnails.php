<?php
 return [
    'gallery' => [
        [
            'default' => 1,
            'name' => '1140x785',
            'width' => 1140,
            'height' => 785,
            'cut' => 'fit'
        ],
        [
            'default' => 0,
            'name' => '1140x121',
            'width' => 1140,
            'height' => 121,
            'cut' => 'fit'
        ],
        [
            'default' => 0,
            'name' => '285x257',
            'width' => 285,
            'height' => 257,
            'cut' => 'fit'
        ]
    ]
 ];