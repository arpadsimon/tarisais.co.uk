<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', 'MainController@index')->name('home');
Route::get('/about', 'MainController@about')->name('about');
Route::get('/gallery', 'MainController@gallery')->name('gallery');
Route::get('/contact', 'MainController@contact')->name('contact');
Route::post('/contact', 'ContactController@store')->name('contactStore');
Route::get('/gallery/show/{group}', 'GalleryGroupController@show')->where('group', '\d+')->name('showGroup');


Route::group(['namespace' => 'Admin', 'prefix' => 'admin', 'middleware' => ['web']], function () {
    Route::get('/login', 'MainController@login')->name('mainLoginAdmin');
    Route::post('/login', 'MainController@login')->name('mainLoginAdmin');

    Route::get('/', 'MainController@index')->name('mainIndexAdmin');
    Route::get('/logout', 'MainController@logout')->name('mainLogoutAdmin');

    Route::get('/galleries', 'GalleryController@index')->name('galleriesAdmin');
    Route::get('/galleries/create', 'GalleryController@create')->name('galleriesAdminCreate');
    Route::post('/galleries/store', 'GalleryController@store')->name('galleriesAdminStore');
    Route::post('/galleries/details/{gallery}', 'GalleryController@details')->name('galleriesAdminDetails')->where('gallery', '\d+');

    Route::get('/gallery_groups', 'GalleryGroupController@index')->name('galleryGroupAdmin');
    Route::get('/gallery_groups/create', 'GalleryGroupController@create')->name('galleryGroupAdminCreate');
    Route::post('/gallery_groups/store', 'GalleryGroupController@store')->name('galleryGroupAdminStore');
    Route::post('/gallery_groups/details/{gallery}', 'GalleryGroupController@details')->name('galleryGroupAdminDetails')->where('gallery', '\d+');

    Route::get('/gallery_photos', 'GalleryPhotoController@index')->name('galleryPhotoAdmin');
    Route::get('/gallery_photos/upload', 'GalleryPhotoController@create')->name('galleryPhotoAdminCreate');
    Route::post('/gallery_photos/store', 'GalleryPhotoController@store')->name('galleryPhotoAdminStore');
    Route::post('/gallery_photos/details/{gallery}', 'GalleryPhotoController@details')->name('galleryPhotoAdminDetails')->where('gallery', '\d+');


});