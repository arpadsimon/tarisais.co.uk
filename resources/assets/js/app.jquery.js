function Site() {
    var _self = this;

    this.init = function () {
        this.cacheElement();
        this.addWidget();
        this.addEvent();
    },
        this.cacheElement = function () {
        },
        this.addEvent = function () {
        },
        this.addWidget = function () {
        }
}

window.siteApp = new Site();

$(document).ready(function () {
    siteApp.init();

    var mySwiper = new Swiper ('.swiper-container', {
        // Optional parameters
        direction: 'horizontal',
        loop: true,
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
    });
});