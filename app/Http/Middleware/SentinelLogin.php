<?php

namespace App\Http\Middleware;

use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Closure;

class SentinelLogin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Sentinel::check() == false && strpos($request->route()->getActionName(),'MainController@login') === false) {
            if(strpos($request->getPathInfo(), 'admin') !== false){
                return redirect()->route('mainLoginAdmin');
            }
        }
        return $next($request);
    }
}
