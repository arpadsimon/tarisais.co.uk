<?php

namespace App\Http\Controllers;

use App\Business\Gallery;
use App\Model\Gallery\Group;
use Illuminate\Http\Request;

class MainController extends Controller
{
    public function index()
    {

        return view('index');
    }

    public function about()
    {
        return view('about');
    }

    public function contact()
    {
        return view('contact');
    }

    public function gallery()
    {
        $groups = Gallery\Group::getActives();
        $thumbnails = Gallery\Group::getThumbnails();
        return view('gallery')->with(compact('groups', 'thumbnails'));
    }
}
