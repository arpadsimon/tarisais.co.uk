<?php

namespace App\Http\Controllers;

use App\Business\Contact;
use App\Http\Requests\ContactStore;
use App\Mail\NewContactMessage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class ContactController extends Controller
{
    public function store(ContactStore $request)
    {
        $message = (new Contact())->storeMessage($request);
        event(new \App\Events\NewContactMessage($message));
        return redirect(route('contact'))->with('message', 'Email sent!');;
    }
}
