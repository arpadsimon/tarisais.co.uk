<?php

namespace App\Http\Controllers\Admin;

use App\Business\User;
use Carbon\Carbon;
use Cartalyst\Sentinel\Checkpoints\NotActivatedException;
use Cartalyst\Sentinel\Checkpoints\ThrottlingException;
use Illuminate\Http\Request;

class MainController extends BaseController
{
    public function index()
    {
        return view('admin.index');
    }
    public function login() {

        if ($this->request->method() == 'POST'){
            try {
                $user = (new User())->authenticate($this->request->only(['email', 'password']));

                if (!$user) {
                    throw new \Exception(trans("general.loginFailed"));
                }

                if($user->is_active != 1) {
                    throw new NotActivatedException();
                }

                if ($user && $user->is_admin == 1) {
                    (new User())->login($user);

                    $this->request->session()->put('site', 'admin');

                    return redirect()->route('mainIndexAdmin');
                } else {
                    throw new \Exception(trans("general.loginFailed"));
                }
            } catch (\Exception $e) {
                $this->request->session()->put("loginError", $e->getMessage());
                return redirect()->route('mainLoginAdmin');
            }
        }
        return view('admin.login');
    }

    public function logout()
    {
        (new User())->logout();

        return redirect()->route('mainLoginAdmin');
    }

}
