<?php
/**
 * Created by PhpStorm.
 * User: arpad
 * Date: 29/08/2018
 * Time: 21:56
 */

namespace App\Http\Controllers\Admin;


use App\Business\Gallery;
use App\Model\Gallery\Group;
use Illuminate\Http\Request;

class GalleryPhotoController extends BaseController
{
    public function index()
    {
        $groups = Gallery\Group::getActives();
        return view('admin.photo.index')->with(compact('groups'));
    }

    public function create()
    {
        $galleryGroups= Gallery\Group::getActives();
        return view('admin.photo.create')->with(compact('galleryGroups'));
    }

    public function store(Request $request)
    {
        try {
           $uploaded = (new Gallery\Photo())->store($request);
           return redirect()->route('galleryPhotoAdmin');
        } catch(\Exception $e) {
            return ['error' => true, 'message' => $e->getMessage()];
        }
    }
}