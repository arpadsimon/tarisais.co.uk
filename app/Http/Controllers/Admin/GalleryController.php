<?php

namespace App\Http\Controllers\Admin;

use App\Business\User as UserBusinnes;
use App\Business\Gallery as GalleryBusinnes;
use App\Http\Requests\StoreGallery;
use App\Model\Gallery;

class GalleryController extends BaseController
{
    public function index()
    {
        $galleries = Gallery::all();
        return view('admin.gallery.index')->with(compact('galleries'));
    }

    public function create()
    {
        return view('admin.gallery.create');
    }

    public function store(StoreGallery $request)
    {
        $user = (new UserBusinnes())->currentUser();
        $save = (new GalleryBusinnes())->save($user, $request);
        return $save;
    }
}
