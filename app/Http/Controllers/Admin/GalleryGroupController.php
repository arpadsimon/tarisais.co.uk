<?php

namespace App\Http\Controllers\Admin;

use App\Business\Gallery;
use App\Business\User as UserBusinnes;
use App\Business\Gallery as GalleryBusinnes;
use App\Model\Gallery\Group;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Illuminate\Http\Request;

class GalleryGroupController extends BaseController
{
    public function index()
    {
        return view('admin.gallery_group.index');
    }

    public function create()
    {
        $galleries = Gallery::getActives();
        return view('admin.gallery_group.create')->with(compact('galleries'));
    }

    public function store(Request $request)
    {
        $user = Sentinel::getUser();
        $save = (new Gallery\Group())->save($user, $request);
        return $save;
    }
}
