<?php
/**
 * Created by PhpStorm.
 * User: arpad
 * Date: 16/08/2018
 * Time: 21:09
 */

namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class BaseController extends Controller
{
    protected $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }
}