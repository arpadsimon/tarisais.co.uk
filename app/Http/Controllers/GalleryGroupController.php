<?php

namespace App\Http\Controllers;

use App\Model\Gallery\Group;
use Illuminate\Http\Request;

class GalleryGroupController extends Controller
{
    public function show(Group $group)
    {
        $header = $group->photos->where('is_header', 1)->first();
        $data = compact('group', 'header');
        return view('gallery.group.show')->with($data);
    }
}
