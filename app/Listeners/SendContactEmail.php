<?php

namespace App\Listeners;

use App\Events\NewContactMessage;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;

class SendContactEmail
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  NewContactMessage  $event
     * @return void
     */
    public function handle(NewContactMessage $event)
    {
        Mail::to(env('CONTACT_EMAIL_ADDRESS'))->send(new \App\Mail\NewContactMessage($event->details));
    }
}
