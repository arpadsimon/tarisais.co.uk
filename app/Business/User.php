<?php
/**
 * Created by PhpStorm.
 * User: arpad
 * Date: 16/08/2018
 * Time: 19:52
 */

namespace App\Business;


use Cartalyst\Sentinel\Laravel\Facades\Sentinel;

class User extends BaseBusiness
{
    public function authenticate($certificate)
    {
        return Sentinel::authenticate($certificate);
    }

    public function login(\App\User $user)
    {
        Sentinel::login($user);
    }

    public function logout()
    {
        Sentinel::logout(null, true);
    }

    public static function currentUser()
    {
        return Sentinel::getUser();
    }

}