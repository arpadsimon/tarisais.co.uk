<?php
/**
 * Created by PhpStorm.
 * User: arpad
 * Date: 29/08/2018
 * Time: 19:10
 */

namespace App\Business\Gallery;


use App\Business\BaseBusiness;
use App\User;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class Photo extends BaseBusiness
{
    public function store($request)
    {
        $savedImages = collect();
        foreach ($request->file('photos') as $photo){

            $is_default = false;
            $is_header = false;

            if(isset($request->is_default) && $request->is_default == 'on') $is_default = true;
            if(isset($request->is_header) && $request->is_header == 'on') $is_header = true;

            $img = $this->saveToDB($photo, $request->gallery_group_id, $is_default, $is_header);

            if(!$is_default){
                $image = Image::make($photo->getRealPath());
                $image->resize(1140, 785, function($constraint) {
                    $constraint->aspectRatio();
                });

                $image->stream();
            } else {
                $image = file_get_contents($photo->getRealPath());
            }

            Storage::disk('local') -> put( $request->gallery_group_id. DIRECTORY_SEPARATOR .$img->name, $image);

            $savedImages->push($img);
        }

        return $savedImages;

    }

    private function saveToDB($photo, $galleryGroup, $is_default = false, $is_header = false)
    {
        $saved = new \App\Model\Gallery\Photo();
        $saved->name = md5(microtime()) . '.' . $photo->getClientOriginalExtension();
        $saved->original_name = $photo->getClientOriginalName();
        $saved->updated_by = Sentinel::getUser()->id;
        if($is_default) $saved->is_default = 1;
        if($is_header) $saved->is_header = 1;
        $saved->save();

        DB::table('gallery_group_gallery_photos')
            ->insert([['gallery_group_id' => $galleryGroup, 'gallery_photo_id' => $saved->id]]);

        return $saved;
    }
}