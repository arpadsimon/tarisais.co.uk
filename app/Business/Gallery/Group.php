<?php
/**
 * Created by PhpStorm.
 * User: arpad
 * Date: 29/08/2018
 * Time: 19:10
 */

namespace App\Business\Gallery;


use App\Business\BaseBusiness;
use App\User;
use Illuminate\Support\Facades\DB;

class Group extends BaseBusiness
{

    public function save(User $user, $request)
    {
        try{
            $galleryGroup = new \App\Model\Gallery\Group();
            $galleryGroup->name = $request->name;
            $galleryGroup->desc = $request->desc;
            $galleryGroup->gallery_id = $request->gallery_id;
            $galleryGroup->updated_by = $user->id;
            $galleryGroup->is_active = 1;
            $galleryGroup->save();
        }catch (\Exception $e) {
            return ["error" => "true", "message" => $e];
        }

        return $galleryGroup;
    }

    public static function getActives()
    {
        return \App\Model\Gallery\Group::where('is_active', 1)->get();
    }

    public static function getThumbnails()
    {
        $thumbnails = [];

        $photos = DB::table('gallery_photos')
            ->select('gallery_photos.id', 'gallery_group_gallery_photos.gallery_group_id', 'gallery_photos.name')
            ->leftJoin('gallery_group_gallery_photos', 'gallery_photos.id', '=', 'gallery_group_gallery_photos.gallery_photo_id')
            ->where('gallery_photos.is_default', 1)
            ->get();

        foreach ($photos as $photo){
            $thumbnails[$photo->gallery_group_id] = $photo->name;
        }

        return $thumbnails;
    }
}