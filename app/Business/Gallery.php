<?php
/**
 * Created by PhpStorm.
 * User: arpad
 * Date: 16/08/2018
 * Time: 19:52
 */

namespace App\Business;

use App\User as UserModel;
use App\Http\Requests\StoreGallery;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;

class Gallery extends BaseBusiness
{
    public static function getActives()
    {
        return \App\Model\Gallery::where('is_active', 1)->get();
    }

    public function save(UserModel $user, StoreGallery $request)
    {
        try{
            $gallery = new \App\Model\Gallery();
            $gallery->name = $request->name;
            $gallery->desc = $request->desc;
            $gallery->code = $request->code;
            $gallery->is_active = 1;
            $gallery->updated_by = $user->id;
            $gallery->save();
        }catch (\Exception $e) {
            return ["error" => "true", "message" => $e];
        }

        return $gallery;
    }
}