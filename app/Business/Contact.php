<?php
/**
 * Created by PhpStorm.
 * User: arpad
 * Date: 16/08/2018
 * Time: 19:52
 */

namespace App\Business;

use App\User as UserModel;
use App\Http\Requests\StoreGallery;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;

class Contact extends BaseBusiness
{
    public function storeMessage($request)
    {
        try{
            $message = new \App\Model\ContactMessage();
            $message->name = $request->name;
            $message->email = $request->email;
            $message->message = $request->message;
            $message->ip = $_SERVER['REMOTE_ADDR'];
            $message->save();
        }catch (\Exception $e) {
            return ["error" => "true", "message" => $e];
        }

        return $message;
    }
}