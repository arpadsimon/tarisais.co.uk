<?php

namespace App\Model;

use App\Model\Gallery\Group;
use Illuminate\Database\Eloquent\Model;

class Gallery extends Model
{
    protected $guarded = [];

    public function groups()
    {
        return $this->hasMany(Group::class);
    }
}
