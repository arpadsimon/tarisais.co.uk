<?php
/**
 * Created by PhpStorm.
 * User: arpad
 * Date: 30/08/2018
 * Time: 19:49
 */

namespace App\Model\Gallery;

use Illuminate\Database\Eloquent\Model;

class Photo extends Model
{
    protected $table = 'gallery_photos';
    protected $guarded = [];

    public function groups()
    {
        return $this->belongsToMany(Group::class, 'gallery_group_gallery_photos', 'gallery_photo_id', 'gallery_group_id');
    }
}
