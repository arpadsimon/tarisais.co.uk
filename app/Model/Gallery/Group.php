<?php

namespace App\Model\Gallery;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    protected $table = 'gallery_groups';
    protected $guarded = [];

    public function gallery()
    {
        return $this->belongsTo(Group::class);
    }

    public function photos()
    {
        return $this->belongsToMany(Photo::class, 'gallery_group_gallery_photos', 'gallery_group_id', 'gallery_photo_id');
    }
}
