<?php

namespace App\Model\Gallery\Group;

use App\Model\Gallery\Group;
use Illuminate\Database\Eloquent\Model;

class Photo extends Model
{
    protected $table = 'gallery_group_gallery_photos';
    protected $guarded = [];

    public function groups()
    {
        return $this->belongsToMany(Group::class, 'gallery_group_gallery_photos', 'gallery_photo_id', 'gallery_group_id');
    }
}
