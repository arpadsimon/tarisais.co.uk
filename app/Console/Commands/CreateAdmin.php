<?php

namespace App\Console\Commands;

use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Illuminate\Console\Command;

class CreateAdmin extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'admin:create';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $credentials = [
            'email'    => 'admin@tarisais.co.uk',
            'password' => 'helikopeter'
        ];

        $user = Sentinel::registerAndActivate($credentials);

        $user->is_active = 1;
        $user->is_admin = 1;
        $user->first_name = 'Admin';
        $user->last_name = 'Admin';
        $user->save();

        $this->info('Default admin created!');
    }
}
