<?php

namespace App\Events;

use App\Model\ContactMessage;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class NewContactMessage
{
    use Dispatchable, InteractsWithSockets, SerializesModels;
    public $details;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(ContactMessage $message)
    {
        $this->details = $message;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('new-message');
    }
}
