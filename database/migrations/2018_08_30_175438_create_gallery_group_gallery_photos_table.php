<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGalleryGroupGalleryPhotosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gallery_group_gallery_photos', function (Blueprint $table) {
            $table->unsignedInteger('gallery_group_id');
            $table->unsignedInteger('gallery_photo_id');

            $table->foreign('gallery_group_id', 'group_id')->references('id')->on('gallery_groups');
            $table->foreign('gallery_photo_id', 'photo_id')->references('id')->on('gallery_photos');

            $table->primary(['gallery_group_id', 'gallery_photo_id'], 'group_photo_primary');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gallery_group_gallery_photos');
    }
}
