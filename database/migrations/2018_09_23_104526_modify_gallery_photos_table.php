<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyGalleryPhotosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('gallery_photos', function (Blueprint $table) {
            $table->tinyInteger('is_default')->nullable();
            $table->tinyInteger('is_header')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('gallery_photos', function (Blueprint $table) {
            $table->dropColumn('is_default');
            $table->dropColumn('is_header');
        });
    }
}
